import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
//import { inject, observer } from 'mobx-react';

import Requests from './screens/requests'
import { View } from 'react-native'

const Navigation = (props:any) => {
    const Stack = createStackNavigator();

    const [active, setActive] = useState('');

    return (
        <NavigationContainer>
          <Stack.Navigator headerMode={'none'} >
              <Stack.Screen name="Requests" component={Requests} />
          </Stack.Navigator>
        </NavigationContainer>
    )
  }

  //export default inject('store')(observer(Navigation))
  export default Navigation
  