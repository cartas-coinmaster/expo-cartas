import React from 'react'
import { View } from 'react-native'
import { Paragraph } from 'react-native-paper'
import { Appbar } from 'react-native-paper'
import App from '../../App'

import useSWR from 'swr'


const fetcher = (url:string) => fetch(url).then(r => r.json())

const requests = () => {

    const { data, error } = useSWR('https://next-cartas.vercel.app/api/request', fetcher)

    console.log(data)

    return(
        <View>
            <Appbar>
                <Appbar.Action icon={'menu'} />
            </Appbar>
            <Paragraph>Teste</Paragraph>
        </View>
    )
}

export default requests